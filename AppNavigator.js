import {createStackNavigator, createAppContainer} from 'react-navigation'

import AllMenu from "./components/AllMenu"
import ChooseMenu from "./components/ChooseMenu"
import Providers from "./components/Providers"

const AppNavigator  = createStackNavigator({
  Providers: {screen: Providers},
  AllMenu: {screen: AllMenu},
  ChooseMenu: {screen: ChooseMenu},
},
{
  initialRouteName: "Providers",
      /* The header config from HomeScreen is now here */
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          flex:1 ,
          color: "#fff",
          fontWeight: 'bold',
          textAlign:"center" ,
          fontSize: 20,
        }  
    }
})

export default createAppContainer(AppNavigator)

