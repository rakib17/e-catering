import React from 'react';
import AppNavigator from "./AppNavigator"
import { Font } from 'expo';
import {Dimensions} from 'react-native';
/* component.js */
import EStyleSheet from 'react-native-extended-stylesheet';

// app entry
let {height, width} = Dimensions.get('window');
EStyleSheet.build({
  $rem: width > 340 ? 18 : 16,
  $primaryColor: '#f4511e',
  $secondaryColor: "#841584"
});

export default class App extends React.Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}
