import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
  bigBlue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
    fontSize: 25
  },
});

export class ChooseMenu extends Component {
  render() {
    return (
      <View>
        <Text  style={styles.bigBlue}>This is the ChooseMenus screen</Text>
        <Text style={styles.red}>This is the ChooseMenu screen</Text>
        <Text>This is the ChooseMenu screen</Text>

        <Text style={styles.red}>This is the ChooseMenu screen</Text>
        <Text style={styles.red}>This is the ChooseMenu screen</Text>
        <Text style={styles.red}>This is the ChooseMenu screen</Text>

      </View>
    )
  }
}

export default ChooseMenu