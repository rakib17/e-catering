import React, { Component } from 'react';
import {  View, Text, Button, Alert } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  button: {
    color: 'red',
    fontSize: "2rem",
  },
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  buttonContainer: {
    width: "80%",
    marginBottom: "1rem"
  }
});

export class Providers extends Component {
    static navigationOptions = {
        title: 'Catering Providers',
      };
    
    onPressButton = () => {
        Alert.alert('You tapped the button!')
    }
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.buttonContainer}>
          <Button
              onPress={() => this.props.navigation.navigate('AllMenu', {
                id: 0
              }) }
              title="Banani Caters"
              color="#841584"
              />
              </View>

        <View style={styles.buttonContainer}>
          <Button
              onPress={() => this.props.navigation.navigate('AllMenu', {
                id: 1
              }) }
              title="Gulshan Caters"
              color="#841584"
              />
              </View>

              <View style={styles.buttonContainer}>
          <Button
              onPress={() => this.props.navigation.navigate('AllMenu', {
                id: 2
              }) }
              title="Dhanmondi Caters"
              color="#841584"
              />
              </View>
      </View>
    )
  }
}

export default Providers