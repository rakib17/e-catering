import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import menu from "../assets/data/menu.json"

const styles = EStyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  title: {
    fontSize: "1.5rem",
    color: "$primaryColor",
    marginVertical: '0.75rem',
    textAlign: "center"
  },
  menuTitle: {
    fontSize: "1.15rem",
    color: "$secondaryColor",
    marginVertical: '0.5rem',
    textAlign: "center"
  },
  textDay: {
    fontSize: "0.75rem",
    marginVertical: '0.15rem',
    flex: 2,
    textAlign: "left"
  },
  textItem: {
    fontSize: "0.75rem",
    marginVertical: '0.15rem',
    flex: 1,
    textAlign: "right"
  },
  menuContainer: {
    flex:1,
    paddingHorizontal: "25%",
    flexDirection: "row",
  }
});

export class AllMenu extends Component {
  render() {
    const id =  this.props.navigation.getParam( "id")
    const days = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
    // console.log( menu[id], "got it")
    return (
      
      <View style={styles.mainContainer}>
        <ScrollView  style={{flex: 1}}>
        <Text style = {styles.title}>{menu[id].name} </Text>
        {menu[id].weeklyMenus.map((menu, mi)=> 
        <View key={mi}>
            <Text style ={styles.menuTitle} key={mi+"a"}>{menu.name} </Text>
            {
              menu.itemByDays.map((item, di) => 
              <View style={styles.menuContainer} key={di}>
                <Text style={styles.textDay}>{days[di]} </Text>
                <Text style={styles.textItem}>{item} </Text>
        </View>

)
}
        </View>

) }
</ScrollView>
      </View>
    )
  }
}

export default AllMenu